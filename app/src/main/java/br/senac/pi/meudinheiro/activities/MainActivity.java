package br.senac.pi.meudinheiro.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import java.util.List;

import br.senac.pi.meudinheiro.R;
import br.senac.pi.meudinheiro.models.Category;
import br.senac.pi.meudinheiro.remote.APIUtils;
import br.senac.pi.meudinheiro.services.CategoryService;
import br.senac.pi.meudinheiro.services.MoneyService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    CategoryService categoryService;
    private ListView lvCategorias;
    private Spinner spinner;

    MoneyService moneyService;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.btn_add).setOnClickListener(addCategory());
        //findViewById(R.id.btn_list_category).setOnClickListener(listaCategorias());
        //spinner = (Spinner) findViewById(R.id.spinner);
        findViewById(R.id.btn_nova_despesa).setOnClickListener(novaDespesa());
        spinner = findViewById(R.id.sp_category_despesa);

        moneyService = APIUtils.getMoneyService();



       // lvCategorias = findViewById(R.id.lv_category);

    }

    private View.OnClickListener novaDespesa() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Call<List<Category>> call = categoryService.getCategories();
                call.enqueue(new Callback<List<Category>>() {

                    @Override
                    public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {
                        if(response.isSuccessful()){
                            ArrayAdapter<String> listData;
                            //listData = new ArrayAdapter<String>(MainActivity.this,android.R.layout.simple_list_item_1, response.body());
                            //s.setAdapter(listData);
                           // spinner.setAdapter(listData);


                        }

                    }

                    @Override
                    public void onFailure(Call<List<Category>> call, Throwable t) {

                    }
                });

            }
        };

    }

    private View.OnClickListener listaCategorias() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Call<List<Category>> call = categoryService.getCategories();
                call.enqueue(new Callback<List<Category>>() {

                    @Override
                    public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {
                        List<Category> posts = response.body();
                        String[] s = new  String[posts.size()];

                        for(int i=0;i<posts.size();i++)
                        {
                            s[i]= posts.get(i).getName();
                            ArrayAdapter<String> a = new ArrayAdapter(MainActivity.this, android.R.layout.simple_list_item_1, s);

                            lvCategorias.setAdapter(a);

                        }

                    }

                    @Override
                    public void onFailure(Call<List<Category>> call, Throwable t) {

                    }
                });
            }
        };
    }

    private View.OnClickListener addCategory() {
        return  new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, CategoryActivity.class);
                startActivity(intent);
            }
        };

    }
}
