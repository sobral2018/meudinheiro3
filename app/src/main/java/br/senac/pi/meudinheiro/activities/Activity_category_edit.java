package br.senac.pi.meudinheiro.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import br.senac.pi.meudinheiro.R;
import br.senac.pi.meudinheiro.adapters.CategoryAdapter;
import br.senac.pi.meudinheiro.models.Category;
import br.senac.pi.meudinheiro.remote.APIUtils;
import br.senac.pi.meudinheiro.services.CategoryService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Activity_category_edit extends AppCompatActivity {

    String id;
    EditText nameCategoria;


    private CategoryAdapter categoryAdapter;

    CategoryService categoryService;
    private ListView lvCategories;
    private List<Category> categoryList;
    Category category;
   // private CategoryAdapter categoryAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_edit);

        setTitle("Atualização de categoria");
        id = this.getIntent().getStringExtra("id");

        findViewById(R.id.btn_update_category).setOnClickListener(catUpdate());
        findViewById(R.id.btn_delete_category).setOnClickListener(deleteCategorie());
        categoryService = APIUtils.getCategoryService();
        //lvCategories = findViewById(R.id.lv_list_categories);
        nameCategoria = findViewById(R.id.edt_category_edit);



        category = new Category();
        res();



        Toast.makeText(this, "ID Capturado: " + id, Toast.LENGTH_SHORT).show();
    }

    private View.OnClickListener deleteCategorie() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Call<Category> call = categoryService.deleteCategory(Integer.parseInt(id));
                call.enqueue(new Callback<Category>() {

                    @Override
                    public void onResponse(Call<Category> call, Response<Category> response) {
                        if(response.isSuccessful()){
                            Toast.makeText(Activity_category_edit.this, "Categoria deletada com sucesso", Toast.LENGTH_LONG).show();

                            Intent intent = new Intent(Activity_category_edit.this, CategoryActivity.class);
                            startActivity(intent);
                        }
                    }

                    @Override
                    public void onFailure(Call<Category> call, Throwable t) {

                    }
                });

            }
        };

    }

    private View.OnClickListener catUpdate() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                category.setName(nameCategoria.getText().toString());
                Call<Category> call = categoryService.updateCategory((Integer.parseInt(id)), category);
                call.enqueue(new Callback<Category>() {

                    @Override
                    public void onResponse(Call<Category> call, Response<Category> response) {
                        if(response.isSuccessful()){
                            Toast.makeText(Activity_category_edit.this, "Categoria atualizada com sucesso", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Category> call, Throwable t) {

                    }
                });
            }
        };
    }


    private void res(){
        Call<Category> call = categoryService.getCategoriesById(Integer.parseInt(id));
        call.enqueue(new Callback<Category>() {
            @Override
            public void onResponse(Call<Category> call, Response<Category> response) {
                if(response.isSuccessful()){
                    nameCategoria.setText(response.body().getName());
                }
            }

            @Override
            public void onFailure(Call<Category> call, Throwable t) {

            }
        });

    }
}
