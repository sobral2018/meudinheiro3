package br.senac.pi.meudinheiro.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import java.util.List;

import br.senac.pi.meudinheiro.R;
import br.senac.pi.meudinheiro.models.Category;
import br.senac.pi.meudinheiro.remote.APIUtils;
import br.senac.pi.meudinheiro.services.CategoryService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class activiry_list_category extends AppCompatActivity {
    CategoryService categoryService;
    //private ListView lvCategorias;
    //private Spinner spinner;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_activiry_list_category);
        categoryService = APIUtils.getCategoryService();

        findViewById(R.id.btn_list_category).setOnClickListener(listaCategorias());
       // spinner = (Spinner) findViewById(R.id.spinner);
        //lvCategorias = findViewById(R.id.)

    }

    private View.OnClickListener listaCategorias() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Call<List<Category>> call = categoryService.getCategories();
                call.enqueue(new Callback<List<Category>>() {

                    @Override
                    public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {
                        List<Category> posts = response.body();
                        String[] s = new  String[posts.size()];
                        for(int i=0;i<posts.size();i++)
                        {
                            s[i]= posts.get(i).getName();
                            final ArrayAdapter a = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_spinner_item, s);
                            a.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            //Setting the ArrayAdapter data on the Spinner
                           // spinner.setAdapter(a);
                        }
                    }

                    @Override
                    public void onFailure(Call<List<Category>> call, Throwable t) {

                    }
                });
            }
        };

    }
}
