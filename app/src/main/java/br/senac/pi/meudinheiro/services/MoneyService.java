package br.senac.pi.meudinheiro.services;



import java.util.List;

import br.senac.pi.meudinheiro.models.Category;
import br.senac.pi.meudinheiro.models.Money;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface MoneyService {

    @GET("money/")
    Call<List<Money>> getMoney();

    @GET("money/{id}/")
    Call<Category> getCategoriesById(@Path("id") int id);

    @POST("money/")
    Call<Category> addCategory(@Body Category category);

    @PUT("money/")
    Call<Category> updateCategory(@Path("id") int id, @Body Category category);

    @DELETE("money/{id}/")
    Call<Category> deleteCategory(@Path("id") int id);

}
